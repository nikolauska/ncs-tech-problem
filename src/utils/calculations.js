// Calculate distance between two points using Euclidean distance
export const calculateDistance = ({x: x1, y: y1}, {x: x2, y: y2}) => {
    const a = x1 - x2;
    const b = y1 - y2;
    return Math.sqrt(a*a + b*b);
}

// Calculate power to between 0-1 where 1 is the closest one
export const calculatePower = (currentPoint, linkStation) => {
    const distance = calculateDistance(currentPoint, linkStation);

    if (distance > linkStation.reach) {
        return 0;
    }

    return Math.pow(linkStation.reach - distance, 2);
}

export const getClosestLinkStation = (x, y, linkStations) => {
    return linkStations
          .map((linkStation) => {
              // Add power to linkstations so we can use reduce to find largest value
              linkStation.power = calculatePower({x: x, y: y}, linkStation)
              return linkStation;
          })
          .reduce((closestLinkStation, linkStation) => {
              // If power is zero it means linkstation is unreachable
              if (linkStation.power == 0) {
                  return closestLinkStation;
              }

              if(!closestLinkStation) {
                  return linkStation;
              }

              if(linkStation.power > closestLinkStation.power) {
                  return linkStation;
              }

              return closestLinkStation;
          }, null);
}
