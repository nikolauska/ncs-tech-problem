import {assert, expect} from 'chai';

import {calculateDistance, calculatePower, getClosestLinkStation} from './calculations';

describe('src/util/calculations', () => {
  describe('calculateDistance', () => {
    it('Should return zero on same point', () => {
        const p1 = {x: 1, y: 1};
        const p2 = {x: 1, y: 1};
        expect(calculateDistance(p1, p2)).to.equal(0);
    });
    it('Should return one when distance is one', () => {
        const p1 = {x: 2, y: 1};
        const p2 = {x: 1, y: 1};
        expect(calculateDistance(p1, p2)).to.equal(1);
    });
  });

  describe('calculatePower', () => {
    it('Should return zero on distance is greaten than reach', () => {
        const point = {x: 20, y: 20};
        const linkstation = {
            'x': 0,
            'y': 0,
            'reach': 10
        }
        expect(calculatePower(point, linkstation)).to.equal(0);
    });
    it('Should return power when within reach reach', () => {
        const point = {x: 1, y: 1};
        const linkstation = {
            'x': 0,
            'y': 0,
            'reach': 10
        }
        expect(calculatePower(point, linkstation) > 0).to.be.true;
    });
    it('Shorter distance to have greater power', () => {
        const p1 = {x: 1, y: 1};
        const p2 = {x: 2, y: 2};
        const linkstation = {
            'x': 0,
            'y': 0,
            'reach': 10
        }
        const pow1 = calculatePower(p1, linkstation);
        const pow2 = calculatePower(p2, linkstation);
        expect(pow1 > pow2).to.be.true;
    });
  });

  describe('getClosestLinkStation', () => {
    it('Should return closest distance if found', () => {
        const linkstations = [
            {
                'x': 0,
                'y': 0,
                'reach': 10
            },
            {
                'x': 10,
                'y': 0,
                'reach': 12
            }
        ]
        const closest = getClosestLinkStation(1, 1, linkstations);
        expect(closest).to.not.be.null;
        expect(closest.x).to.equal(0);
        expect(closest.y).to.equal(0);
    });
    it('Should return null if no link stations found', () => {
        const linkstations = [
            {
                'x': 0,
                'y': 0,
                'reach': 10
            },
            {
                'x': 10,
                'y': 0,
                'reach': 12
            }
        ]
        const closest = getClosestLinkStation(50, 50, linkstations);
        expect(closest).to.be.null;
    });
  });

});
