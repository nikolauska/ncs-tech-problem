import React from 'react';
import ReactDOM from 'react-dom';

import LinkStationStatic from './components/LinkStationStatic';

const App = () => {
    return (
        <div>
          <LinkStationStatic x={0} y={0} />
          <LinkStationStatic x={100} y={100} />
          <LinkStationStatic x={15} y={10} />
          <LinkStationStatic x={18} y={18} />
        </div>
    )
}

ReactDOM.render(<App/>, document.getElementById('app'));
