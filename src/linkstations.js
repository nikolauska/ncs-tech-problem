// Define all available link stations
const linkStationList = [
    {
        'x': 0,
        'y': 0,
        'reach': 10
    },
    {
        'x': 20,
        'y': 20,
        'reach': 5
    },
    {
        'x': 10,
        'y': 0,
        'reach': 12
    }
]

export default linkStationList;
