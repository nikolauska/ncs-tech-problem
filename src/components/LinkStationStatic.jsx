/* This component will calculate closest distance
 * and show info about the closest usable
 * link station
 */
import React from 'react';

import linkStationList from '../linkstations';
import { getClosestLinkStation } from '../utils/calculations';

const LinkStationStatic = ({x, y}) => {
    if(typeof(x) !== 'number') {
        if(!x) {
            return (
                <p>Missing value for X</p>
            );
        }

        x = Number(x);
        if(isNaN(x)) {
            return (
                <p>Invalid value for X</p>
            );
        }
    }

    if(typeof(y) !== 'number') {
        if(!y) {
            return (
                <p>Missing value for Y</p>
            );
        }

        y = Number(y);
        if(isNaN(y)) {
            return (
                <p>Invalid value for Y</p>
            );
        }
    }

    const closestLink = getClosestLinkStation(x, y, linkStationList);
    if (!closestLink) {
        return (
            <p>No link station within reach for point {x},{y}</p>
        );
    } else {
        return (
            <p>Best link station for point {x},{y} is {closestLink.x},{closestLink.y} with power {closestLink.power}</p>
        );
    }
}

export default LinkStationStatic;
